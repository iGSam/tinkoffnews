//
//  ArticleViewController.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!

    let content = ArticleContent()

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        content.onUpdate = { [weak self] in
            self?.updateText()
        }
        content.load()
    }

    // MARK: - Updates

    private func updateText() {
        spinner.stopAnimating()
        webView.loadHTMLString(content.html ?? "", baseURL: nil)
    }

}
