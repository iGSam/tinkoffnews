//
//  NewsArticle+Extensions.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

extension NewsArticle {

    static func id(with json: JSON) -> String? {
        return json["id"] as? String
    }

    func configure(with json: JSON) {
        if let id = NewsArticle.id(with: json),
            let caption = json["text"] as? String,
            let publicationDate = json["publicationDate"] as? JSON,
            let milliseconds = publicationDate["milliseconds"] as? Double {

            self.id = id
            self.caption = caption
            self.publicationDate = milliseconds
        }

        if let text = json["content"] as? String {
            self.text = text
        }
    }

}
