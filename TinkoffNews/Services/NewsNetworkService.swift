//
//  NewsNetworkService.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

private enum NewsUrl {

    case news
    case newsArticle(id: String)

    var url: String {
        switch self {
        case .news:
            return "news/"
        case .newsArticle(let id):
            return "news_content?id=\(id)"
        }
    }

}

class NewsNetworkService {

    let cache = NewsCacheService()

    func requestNews(completion: @escaping ([NewsArticle]) -> Void) {
        Api.defaultApi.request(urlString: NewsUrl.news.url) { [weak self] apiResponse in
            if let jsonObjects = apiResponse.jsonObjects {
                self?.cache.saveNews(from: jsonObjects) { newsArticles in
                    completion(newsArticles)
                }
            }
        }
    }

    func requestArticle(with id: String, completion: @escaping (NewsArticle?) -> Void) {
        Api.defaultApi.request(urlString: NewsUrl.newsArticle(id: id).url) { [weak self] apiResponse in
            if let json = apiResponse.json {
                self?.cache.saveArticle(with: id, from: json) { newsArticle in
                    completion(newsArticle)
                }
            }
        }
    }

}
