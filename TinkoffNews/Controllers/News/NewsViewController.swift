//
//  NewsViewController.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController,
                          UITableViewDataSource,
                          UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    private let cellReuseIdentifier = "NewsCaptionCell"

    private let content = NewsContent()

    var isLoading: Bool = true {
        didSet {
            updateLoadingState()
        }
    }

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureNavigationBar()

        content.fetchFromCache()
        loadNews()
    }

    // MARK: - Config

    func configureTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)

        tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
    }

    func configureNavigationBar() {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.mainDarkGray
        self.navigationController?.navigationBar.barTintColor = UIColor.mainYellow
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainDarkGray]
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.items.count
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)

        if let captionCell = cell as? NewsCaptionCell {
            captionCell.headline = content.newsCaption(for: indexPath.row)
        }

        return cell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sender = sender as? UITableViewCell,
              let indexPath = tableView.indexPath(for: sender),
              let articleViewController = segue.destination as? ArticleViewController else {
                return
        }
        articleViewController.content.id = content.items[indexPath.row].id

        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.tableView.deselectRow(at: indexPath, animated: true)
        })

        isLoading = false
    }

    // MARK: - Actions

    @objc private func refresh() {
        loadNews()
    }

    // MAKR: - Updates

    private func loadNews() {
        isLoading = true
        content.onItemsUpdate = { [weak self] in
            self?.updateTable()
        }
        content.load()
    }

    private func updateTable() {
        isLoading = false
        tableView.reloadData()
    }

    private func updateLoadingState() {
        if isLoading == false {
            spinner.stopAnimating()
            tableView.isHidden = false
            tableView.refreshControl?.endRefreshing()
        }
    }
}
