//
//  String+Extensions.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import Foundation
import UIKit

extension String {

    var plainText: String {
        guard let encodedData = self.data(using: .utf8) else {
            return self
        }

        do {
            let decodedString = try NSAttributedString(data: encodedData,
                                                       options: [
                                                        .documentType: NSAttributedString.DocumentType.html,
                                                        .characterEncoding: String.Encoding.utf8.rawValue],
                                                       documentAttributes: nil)
            return decodedString.string
        }
        catch {
            print("Can't make plain text from html: \(error.localizedDescription)")
            return self
        }
    }

    var completeHtml: String {
        return "<html><head><title></title></head><body style=\"background:transparent;\">" + self + "</body></html>"
    }

}
