//
//  NewsContent.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class NewsContent {

    private(set) var items: [NewsArticle] = []

    var onItemsUpdate: (() -> Void)?

    private let newsNetworkService = NewsNetworkService()

    func newsCaption(for index: Int) -> String? {
        return items[index].caption?.plainText
    }

    // MARK: - Loading data

    func fetchFromCache() {
        newsNetworkService.cache.newsItems() { [weak self] newsItems in
            self?.items = newsItems
            if !newsItems.isEmpty {
                self?.onItemsUpdate?()
            }
        }
    }

    func load() {
        newsNetworkService.requestNews() { [weak self] newsItems in
            self?.items = newsItems
            self?.onItemsUpdate?()
        }
    }

}
