//
//  NewsCacheService.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit
import CoreData

private let sortKey = "publicationDate"

class NewsCacheService {

    let mainContext: NSManagedObjectContext
    let backgroundContext: NSManagedObjectContext

    init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        mainContext = appDelegate.persistentContainer.viewContext
        backgroundContext = appDelegate.persistentContainer.newBackgroundContext()
    }

    // MARK: - Reading

    func newsArticle(with id: String) -> NewsArticle? {
        return fetch(newsArticleFetchRequest(with: id)).first
    }

    func newsItems(completion: @escaping ([NewsArticle]) -> ()) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let backgroundReadingContext = appDelegate.persistentContainer.newBackgroundContext()

        backgroundReadingContext.perform { [unowned self] in
            _ = self.fetch(self.newsItemsFetchRequest, context: backgroundReadingContext)
            DispatchQueue.main.async {
                completion(self.fetch(self.newsItemsFetchRequest))
            }
        }
    }

    private func newsArticleFetchRequest(with articleId: String) -> NSFetchRequest<NewsArticle> {
        let fetchRequest: NSFetchRequest<NewsArticle> = NewsArticle.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", articleId)
        fetchRequest.fetchLimit = 1
        return fetchRequest
    }

    private var newsItemsFetchRequest: NSFetchRequest<NewsArticle> {
        let fetchRequest: NSFetchRequest<NewsArticle> = NewsArticle.fetchRequest()
        let sort = NSSortDescriptor(key: sortKey, ascending: false)
        fetchRequest.sortDescriptors = [sort]
        return fetchRequest
    }

    private func fetch(_ request: NSFetchRequest<NewsArticle>,
                       context: NSManagedObjectContext? = nil) -> [NewsArticle] {
        do {
            return try (context ?? mainContext).fetch(request)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return []
        }
    }

    // MARK: - Writing

    func saveNews(from jsonObjects: [Any], completion: @escaping ([NewsArticle]) -> Void) {
        backgroundContext.perform { [unowned self] in
            let oldIds = self.fetch(self.newsItemsFetchRequest,
                                    context: self.backgroundContext).compactMap { $0.id }

            for case let json as JSON in jsonObjects {
                if let id = NewsArticle.id(with: json),
                    !oldIds.contains(id) {
                    let newsArticle = NewsArticle(context: self.backgroundContext)
                    newsArticle.configure(with: json)
                }
            }

            self.saveContext {
                completion(self.fetch(self.newsItemsFetchRequest))
            }
        }
    }

    func saveArticle(with id: String, from json: JSON, completion: @escaping (NewsArticle?) -> Void) {
        mainContext.perform { [unowned self] in
            guard let newsArticle = self.fetch(self.newsArticleFetchRequest(with: id)).first else {
                return
            }

            newsArticle.configure(with: json)

            completion(self.fetch(self.newsArticleFetchRequest(with: id)).first)
        }
    }

    private func saveContext(completion: @escaping () -> Void) {
        do {
            try backgroundContext.save()
            mainContext.performAndWait { [unowned self] in
                do {
                    try self.mainContext.save()

                    DispatchQueue.main.async {
                        completion()
                    }
                } catch {
                    fatalError("Failure to save context: \(error)")
                }
            }
        } catch {
            fatalError("Failure to save context: \(error.localizedDescription)")
        }
    }

}
