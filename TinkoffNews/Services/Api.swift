//
//  Api.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

typealias JSON = [String: Any]

private let baseUrlString = "https://api.tinkoff.ru/v1/"

class Api {

    static let defaultApi = Api()

    func request(urlString: String, completion: @escaping (ApiResponse) -> Void) {
        guard let url = URL(string: baseUrlString + urlString) else {
            return
        }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data,
                let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
                let json = jsonObject as? JSON,
                let apiResponse = ApiResponse(json: json),
                apiResponse.isOk == true {
                DispatchQueue.main.async {
                    completion(apiResponse)
                }
            } else {
                print("Can't load data")
            }

        }.resume()
    }

}
