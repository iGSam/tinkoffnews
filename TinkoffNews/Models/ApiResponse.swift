//
//  ApiResponse.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class ApiResponse {

    private static let resultCodeOk = "OK"

    let resultCode: String?
    let errorMessage: String?
    let payload: Any?

    var isOk: Bool {
        return resultCode == ApiResponse.resultCodeOk
    }

    var jsonObjects: [Any]? {
        return payload as? [Any]
    }

    var json: JSON? {
        return payload as? JSON
    }

    init?(json: JSON) {
        guard let resultCode = json["resultCode"] as? String else {
            return nil
        }

        self.resultCode = resultCode
        errorMessage = json["errorMessage"] as? String
        payload = json["payload"]
    }

}
