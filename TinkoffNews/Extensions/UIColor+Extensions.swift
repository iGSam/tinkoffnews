//
//  UIColor+Extensions.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import Foundation

import UIKit

extension UIColor {

    public class var mainYellow: UIColor {
        return UIColor(hex6: 0xFEDD2D)
    }

    public class var mainDarkGray: UIColor {
        return UIColor(hex6: 0x3D4656)
    }

    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / 0xFF
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / 0xFF
        let blue    = CGFloat((hex6 & 0x0000FF) >>  0) / 0xFF

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

}
