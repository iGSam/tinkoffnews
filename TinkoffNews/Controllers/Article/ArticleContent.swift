//
//  ArticleContent.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class ArticleContent {

    var onUpdate: (() -> Void)?

    private let newsNetworkService = NewsNetworkService()

    var id: String?
    private var newsArticle: NewsArticle?
    var html: String? {
        return newsArticle?.text?.completeHtml
    }

    func load() {
        guard let id = id else {
            return
        }

        newsArticle = newsNetworkService.cache.newsArticle(with: id)
        if newsArticle?.text != nil {
            onUpdate?()
        }

        newsNetworkService.requestArticle(with: id) { [weak self] newsArticle in
            self?.newsArticle = newsArticle
            self?.onUpdate?()
        }
    }

}
