//
//  NewsCell.swift
//  TinkoffNews
//
//  Created by Dmitry Labetsky on 8/3/17.
//  Copyright © 2017 DmiLab. All rights reserved.
//

import UIKit

class NewsCaptionCell: UITableViewCell {

    @IBOutlet weak var headlineLabel: UILabel!

    var headline: String? {
        didSet {
            if headline != oldValue {
                updateHeadline()
            }
        }
    }

    // MARK: - Updates

    func updateHeadline() {
        headlineLabel.text = headline
    }

}
